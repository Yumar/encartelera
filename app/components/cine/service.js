(function () {
    'use strict';

    angular.module('enCine')
            .factory('$cine',['$firebaseArray', '$firebaseObject', 'FIREBASE_URL', function ($firebaseArray, $firebaseObject, FIREBASE_URL) {
                var db = new Firebase(FIREBASE_URL);
                var cines = $firebaseArray(db.child('cines'));

                var $cines = {
                    list: cines,
                    create: function (cine){
                        return cines.$add(cine);
                    },
                    get: function (id){
                        return $firebaseObject(db.child('cines').child(id));
                    },
                    delete: function(cine){
                        return cines.$remove(cine);
                    }
                };
                return $cines;
            }]);
})();