(function () {
    'use strict';
//TODO: refactor services for loading data on view resolve.
    angular.module('enCine')
            .factory('$pelicula', ['$firebaseArray', '$firebaseObject', 'FIREBASE_URL', function ($firebaseArray, $firebaseObject, FIREBASE_URL) {
                    var db = new Firebase(FIREBASE_URL);
                    var peliculas = $firebaseArray(db.child('peliculas'));

                    var $pelicula = {
                        list: peliculas,
                        create: function (pelicula) {
                            return peliculas.$add(pelicula);
                        },
                        get: function (id) {
                            return $firebaseObject(db.child('peliculas').child(id));
                        },
                        delete: function (pelicula) {
                            return peliculas.$remove(pelicula);
                        }
                    };
                    return $pelicula;
                }]);
})();