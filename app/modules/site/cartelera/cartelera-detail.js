(function (){
    'use strict';
    angular.module('cartelera-detail', [])
            .controller('CarteleraDetailController', function ($scope, $stateParams, $pelicula){              
                $scope.pelicula = $pelicula.get($stateParams.idpelicula);
            });
})();


