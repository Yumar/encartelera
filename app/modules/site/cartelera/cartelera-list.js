(function () {
    'use strict';
    angular.module('cartelera-list', [])
            .controller('CarteleraListController', ['$scope', '$pelicula', function ($scope, $pelicula) {
                $scope.peliculas = $pelicula.list;
            }]);
})();

