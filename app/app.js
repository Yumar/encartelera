(function () {
    'use strict';

    angular.module('enCine', [
        'site'
                , 'mm.foundation'
                , 'firebase'
                , 'angular.filter'
    ])
            .constant('FIREBASE_URL', 'http://encineapp.firebaseio.com');
})();